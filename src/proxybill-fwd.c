/*
 * ProxyBill fw control daemon - proxybill-fwd
 *
 * Copyright (c) 2006, Reto Buerki <reet@codelabs.ch>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * This program is distributed under the terms of the GNU General Public
 * Licence.  See the file COPYING for details.
 *
 */

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/time.h>
#include <string.h>
#include <syslog.h>
#include "setuid.h"
#include "proxybill-fwd.h"

/*
 * signal handler
 */
void changehandler(int sig)
{
	if (sig == SIGRTMIN+1) {
		if (is_writing) {
			syslog(LOG_DEBUG, "Daemon credit dump in progress ... ignoring");
			return;
		}
		if (!read_credits()) {
			syslog(LOG_ERR, "Could not read user credits ... going down");
			exit(EXIT_FAILURE);
		}
		if (!check_credits(true)) {
			syslog(LOG_ERR, "Could not update fw ruleset ... going down");
			exit(EXIT_FAILURE);
		}
	}
	if (sig == SIGALRM) {
		syslog(LOG_DEBUG, "Timer wake up event");
		check_credits(false);
	}
	if (sig == SIGUSR1) { dump_credits(); }
	if (sig == SIGUSR2) { fdump_credits(DUMPFILE); }
	if (sig == SIGTERM || sig == SIGINT) {
		syslog(LOG_INFO, "Quit received ... cleaning up");
		if (!clean_system()) {
			syslog(LOG_ERR, "Could not clean up fw chains");
			exit(EXIT_FAILURE);
		}
		exit(EXIT_SUCCESS);
	}
	if (sig == SIGHUP) {
		syslog(LOG_INFO, "SIGHUP received ... restarting");
		if (!clean_system()) {
			syslog(LOG_ERR, "Could not clean up fw chains");
			exit(EXIT_FAILURE);
		}
		if (!init_system()) {
			syslog(LOG_ERR, "Could not init fw system ... going down");
			exit(EXIT_FAILURE);
		}
		if (!read_credits()) {
			syslog(LOG_ERR, "Could not read user credits ... going down");
			exit(EXIT_FAILURE);
		}
		if (!check_credits(true)) {
			syslog(LOG_ERR, "Could not update fw ruleset ... going down");
			exit(EXIT_FAILURE);
		}
	}
}

int main(int argc, char **argv)
{
	/* register handlers */

	/* SIGRTMIN + 1 */
	signal(SIGRTMIN+1, changehandler);
	int fd;
	/* open our directory to watch */
	fd = open(WATCHDIR, O_RDONLY);
	fcntl(fd, F_SETSIG, SIGRTMIN + 1);
	fcntl(fd, F_NOTIFY, DN_MODIFY|DN_CREATE|DN_MULTISHOT);

	/* SIGALRM */
	signal(SIGALRM, changehandler);
	struct itimerval value, ovalue;
	int which = ITIMER_REAL;
	value.it_interval.tv_sec = 30;
	value.it_interval.tv_usec = 0;
	value.it_value.tv_sec = 0;
	value.it_value.tv_usec = 500;

	/* SIGUSR[1,2] */
	signal(SIGUSR1, changehandler);
	signal(SIGUSR2, changehandler);

	/* SIGHUP */
	signal(SIGHUP, changehandler);

	/* SIGTERM, SIGINT */
	signal(SIGINT, changehandler);
	signal(SIGTERM, changehandler);
	printf("*** proxybill-fw handler starting ...\n");

	/* read user credits */
	if (!read_credits()) {
		syslog(LOG_ERR, "Could not read user credits ... exiting");
		exit(EXIT_FAILURE);
	}
	/* init proxybill fw system */
	if (!init_system()) {
		syslog(LOG_ERR, "Could not init fw system ... exiting");
		exit(EXIT_FAILURE);
	}
	/* init user rules */
	if (!check_credits(true)) {
		syslog(LOG_ERR, "Could not init fw ruleset ... exiting");
		exit(EXIT_FAILURE);
	}
	/* activate timer signal */
	if (setitimer( which, &value, &ovalue ) < 0) {
		syslog(LOG_ERR, "Could not set wake up timer ... exiting");
		return(EXIT_FAILURE);
	}
	/* enter main loop */
	while(1) {
		pause();
	}
	exit(EXIT_SUCCESS);
}

/*
 * update proxybill fw rules
 * -> flushes chains first
 */
int update_fw()
{
	char command[STRING_SIZE];

	exec_command(IPTABLES " -F PB_C_F");

	unsigned int l = 0;
	while (strlen(credits[l].username) > 0) {
		if (credits[l].state != true) {
			l++;
			continue;
		}
		// TODO: error checking
		memset(command, 0, STRING_SIZE);
		snprintf(command, STRING_SIZE - 1, IPTABLES
				" -I PB_C_F -s %s -o %s -j ACCEPT",
				credits[l].ip,
				red_iface.name);
		exec_command(command);
		l++;
	}
	return true;
}

/*
 * read user credits
 * from proxybill credits file
 */
int read_credits()
{
	FILE *credit_f;
	unsigned int fd;
	char s[STRING_SIZE] = "";
	unsigned int c;
	char *result;

	fd = open(CREDITSFILE, O_RDWR);
	if (lock_file(fd) == -1) {
		syslog(LOG_ERR, "Could not lock proxybill credits file '%s'",
				CREDITSFILE);
		return false;
	}

	if ((credit_f = fdopen(fd, "r")) == NULL) {
		syslog(LOG_ERR, "Could not open proxybill credits file '%s'",
				CREDITSFILE);
		return false;
	}
	/* clear credit struct */
	memset(credits, 0, sizeof(credits));
	/* (re)read credits */
	unsigned int l = 0;
	while (fgets(s, STRING_SIZE, credit_f) != NULL &&
			l <= MAXUSER) {
		if (s[strlen(s) - 1] == '\n')
			s[strlen(s) - 1] = '\0';
		c = 0;
		result = strtok(s, ",");
		/* read all credit entries */
		while (result)
		{
			/* state */
			if (c == 0)
				credits[l].state = atoi(result);
			/* username */
			if (c == 1)
				snprintf(credits[l].username,
						sizeof(credits[l].username),
						"%s", result);
			/* credit (seconds) */
			if (c == 2)
				credits[l].credit = atoi(result);
			/* password (hash) */
			if (c == 3)
				snprintf(credits[l].pw,
						sizeof(credits[l].pw),
						"%s", result);
			/* timestamp */
			if (c == 4)
				credits[l].timestamp = atoi(result);
			/* ip */
			if (c == 5) {
				// TODO: VALID_IP makro does not work?!
				if (credits[l].state && !VALID_IP(result)) {
					syslog(LOG_ERR, "Bad source IP: %s", result);
					fclose(credit_f);
					return false;
				}
				snprintf(credits[l].ip,
						sizeof(credits[l].ip),
						"%s", result);
			}
            /* plain pw */
            if (c == 6) {
                snprintf(credits[l].plain,
                        sizeof(credits[l].plain),
                        "%s", result);
            }
			c++;
			result = strtok(NULL, ",");
		}
		l++;
	}
	/* free lock */
	unlock_file(fd);
	fclose(credit_f);
	return true;
}

/*
 * check user credits
 * -> remove rules if user credit
 *    expired
 */
int check_credits(unsigned int clear) {
	time_t t;
	time( &t );
	syslog(LOG_DEBUG, "Checking user credits ...");
	syslog(LOG_DEBUG, "Current timestamp: %d", t);
	unsigned int l = 0;
	unsigned int changes = false;
	while (strlen(credits[l].username) > 0) {
		if (credits[l].state != true) {
			l++;
			continue;
		}
		int timestamp = credits[l].timestamp;
		unsigned int delta = credits[l].credit;
		if (timestamp + delta <= t) {

			char command[STRING_SIZE];
			credits[l].state = expired;

			if (!changes)
				changes = true;

			if (clear)
				continue;

			memset(command, 0, STRING_SIZE);
			snprintf(command, STRING_SIZE - 1, IPTABLES
					" -D PB_C_F -s %s -o %s -j ACCEPT",
					credits[l].ip,
					red_iface.name);
			exec_command(command);

		}
		syslog(LOG_DEBUG, " �--- Credits for '%s':%s:%d (s)",
				credits[l].username, state[credits[l].state],
                credits[l].credit);
		l++;
	}
	if (clear)
		update_fw();
	if (changes) {
		is_writing = true;
		fdump_credits(CREDITSFILE);
		is_writing = false;
	}

	return true;
}

/*
 * dump user credits to syslog
 */
void dump_credits()
{
	syslog(LOG_INFO, "Credits dump called ... dumping ...");
	unsigned int l = 0;
	while (strlen(credits[l].username) > 0) {
		syslog(LOG_INFO, "[*] name     : %s\n", credits[l].username);
		syslog(LOG_INFO, " __state     : %s\n", state[credits[l].state]);
		syslog(LOG_INFO, " __credit    : %d\n", credits[l].credit);
		syslog(LOG_INFO, " __timestamp : %d\n", credits[l].timestamp);
		syslog(LOG_INFO, " __ipAddr    : %s\n", credits[l].ip);
		l++;
	}
}

/*
 * dump user credits to file
 */
int fdump_credits(char *file)
{
	FILE* dump_f;
	unsigned int fd;

	syslog(LOG_INFO, "Credits fdump called ... dumping to '%s' ...", file);
	fd = open(file, O_RDWR | O_CREAT | O_TRUNC, 0600);
	if (fd == -1) {
		syslog(LOG_ERR, "Could not open() file '%s'", file);
		return false;
	}
	if (lock_file(fd) == -1) {
		syslog(LOG_ERR, "Could not lock() file '%s', file");
		return false;
	}
	if ((dump_f = fdopen(fd, "w+")) == NULL) {
		syslog(LOG_ERR, "Could not fdopen() file '%s'", file);
		return false;
	}
	/* write credits to file */
	unsigned int l = 0;
	while (strlen(credits[l].username) > 0) {
		char line[STRING_SIZE];
		snprintf(line, STRING_SIZE - 1, "%d,%s,%d,%s,%d,%s,%s\n",
				credits[l].state,
				credits[l].username,
				credits[l].credit,
				credits[l].pw,
				credits[l].timestamp,
				credits[l].ip,
                credits[l].plain);
		fputs(line, dump_f);
		l++;
	}
	syslog(LOG_INFO, "%d credits dumped to '%s'", l, file);
	if (unlock_file(fd) == -1) {
		syslog(LOG_ERR, "Could not free lock on file '%s'", file);
		return false;
	}

	fclose(dump_f);
	return true;
}

/*
 * read settings and init
 * proxybill fw
 */
int init_system()
{
	char net[NET];

	/* read interface information */
	if (!read_iface_info()) {
		syslog(LOG_ERR, "Could not get iface info ...");
		return false;
	}

	/* get client network */
	get_setting(SETTINGFILE, "CNET", net);
	if (net == NULL) {
		syslog(LOG_ERR, "Could not read client network");
		return false;
	}

	/* init firewall */

    /* create all needed chains */
	exec_command(IPTABLES " -N PB_I");
	exec_command(IPTABLES " -N PB_F");
	exec_command(IPTABLES " -N PB_C_F");
    exec_command(IPTABLES " -N LOG_PB");

    /* init LOG_PB chain */
    exec_command(IPTABLES " -A LOG_PB -m limit --limit 10/minute "
            "-j LOG --log-prefix 'proxybill '");

    /* insert PB chains into INPUT / FORWARD chains */
	char command[STRING_SIZE];
	memset(command, 0, STRING_SIZE);
	snprintf(command, STRING_SIZE - 1, IPTABLES " -I INPUT -s %s/%s -j PB_I",
			client_iface.netaddress, client_iface.netmask);
	exec_command(command);

	memset(command, 0, STRING_SIZE);
	snprintf(command, STRING_SIZE - 1, IPTABLES " -I FORWARD -s %s/%s -j PB_F",
			client_iface.netaddress, client_iface.netmask);
	exec_command(command);

	exec_command(IPTABLES " -I PB_F -j PB_C_F");

    exec_command(IPTABLES " -A PB_I -j LOG_PB");
    exec_command(IPTABLES " -A PB_I -j DROP");
    exec_command(IPTABLES " -A PB_F -j LOG_PB");
    exec_command(IPTABLES " -A PB_F -j DROP");

	/* make sure ipcop webgui is still reachable */
	memset(command, 0, STRING_SIZE);
	snprintf(command, STRING_SIZE -1, IPTABLES
			" -I PB_I -p tcp --dport 445 -i %s -d %s -j ACCEPT",
			client_iface.name, client_iface.address);
	exec_command(command);

    /* XXX: workaround for BLUE network.
     *
     * the ipcop hostname always resolves to GREEN iface address, so we must
     * allow access to GREEN ipcop ip from BLUE. Will be removed when captive
     * proxy with automatic redirect is implemented.
     */
    if (is_blue) {
        syslog(LOG_INFO, "Operating on BLUE_NET, enabling workaround");

        memset(command, 0, STRING_SIZE);
        snprintf(command, STRING_SIZE -1, IPTABLES
                " -I PB_I -p tcp --dport 445 -i %s -d %s -j ACCEPT",
                client_iface.name, green_iface.address);
        exec_command(command);
    }

    /* allow DNS queries to ipcop */
	memset(command, 0, STRING_SIZE);
	snprintf(command, STRING_SIZE -1, IPTABLES
			" -I PB_I -p udp --dport 53 -i %s -d %s -j ACCEPT",
			client_iface.name, client_iface.address);
	exec_command(command);

	return true;
}

/*
 * read interface info/IPs
 */
int read_iface_info() {

	/* we need the red interface for output */
	if (!get_value(REDDEVFILE, red_iface.name, sizeof(red_iface.name)))
		return false;
	if (!get_setting(IFACEFILE, "RED_ADDRESS", red_iface.address))
		return false;

    /* we need green interface information when operating on BLUE */
	if (!get_setting(IFACEFILE, "GREEN_DEV", green_iface.name))
		return false;
	if (!get_setting(IFACEFILE, "GREEN_NETMASK", green_iface.netmask))
		return false;
	if (!get_setting(IFACEFILE, "GREEN_ADDRESS", green_iface.address))
		return false;
	if (!get_setting(IFACEFILE, "GREEN_NETADDRESS", green_iface.netaddress))
		return false;

	/* get client network */
	char iface[5];
	char search[15];
	if (!get_setting(SETTINGFILE, "CNET", iface))
		return false;
	snprintf(search, sizeof(search), "%s_DEV", iface);
	if (!get_setting(IFACEFILE, search, client_iface.name))
		return false;
	snprintf(search, sizeof(search), "%s_NETMASK", iface);
	if (!get_setting(IFACEFILE, search, client_iface.netmask))
		return false;
	snprintf(search, sizeof(search), "%s_ADDRESS", iface);
	if (!get_setting(IFACEFILE, search, client_iface.address))
		return false;
	snprintf(search, sizeof(search), "%s_NETADDRESS", iface);
	if (!get_setting(IFACEFILE, search, client_iface.netaddress))
		return false;

	/* verify devices */
	if (!VALID_DEVICE(red_iface.name))
	{
		syslog(LOG_ERR, "Not a valid RED interface: %s",
				red_iface.name);
		return false;
	}

	if (!VALID_DEVICE(green_iface.name))
	{
		syslog(LOG_ERR, "Not a valid GREEN interface: %s",
				green_iface.name);
		return false;
	}

	if (!VALID_DEVICE(client_iface.name))
	{
		syslog(LOG_ERR, "Not a valid client interface: %s",
				client_iface.name);
		return false;
	}
	syslog(LOG_INFO, "CLIENT_NET: %s (%s)", client_iface.name,
			client_iface.address);
	syslog(LOG_INFO, "RED_NET: %s (%s)", red_iface.name,
			red_iface.address);
    /* are we on the BLUE network? */
    if (strncmp(iface, "BLUE", 4) == 0) { is_blue = true; }

	return true;
}

/*
 * delete proxybill fw chains
 */
int clean_system()
{
	/* flush all chains */
	exec_command(IPTABLES " -F PB_I");
	exec_command(IPTABLES " -F PB_F");
	exec_command(IPTABLES " -F PB_C_F");
	exec_command(IPTABLES " -F LOG_PB");

	/* delete jumps to PB */
	char command[STRING_SIZE];
	memset(command, 0, STRING_SIZE);
	snprintf(command, STRING_SIZE - 1, IPTABLES " -D INPUT -s %s/%s -j PB_I",
			client_iface.netaddress, client_iface.netmask);
	exec_command(command);

	memset(command, 0, STRING_SIZE);
	snprintf(command, STRING_SIZE - 1, IPTABLES " -D FORWARD -s %s/%s -j PB_F",
			client_iface.netaddress, client_iface.netmask);
	exec_command(command);

    /* delete our chains */
	exec_command(IPTABLES " -X PB_I");
	exec_command(IPTABLES " -X PB_F");
	exec_command(IPTABLES " -X PB_C_F");
	exec_command(IPTABLES " -X LOG_PB");

	// TODO: error checking
	return true;
}

/*
 * read setting without key from file
 */
int get_value(char* file, char* value, int maxchars) {

	FILE *f = NULL;

	f = fopen(file, "r");
	if (f == NULL) {
		return false;
	}

	/* read at most one less than maxchars chars from file into value */
	if (fgets(value, maxchars, f) == NULL) {
		fclose(f);
		return false;
	}
	fclose(f);
	return true;
}

/*
 * return a settings value
 */
int get_setting(char *file, char *name, char *value) {

	int fd;
	FILE *f;
	char s[STRING_SIZE] = "";
	char *param, *e;

	fd = open(file, O_RDWR);
	if (lock_file(fd) == -1) {
		syslog(LOG_ERR, "Could not lock file '%s'",
				file);
		return false;
	}

	if ((f = fdopen(fd, "r")) == NULL) {
		syslog(LOG_ERR, "Could not open file '%s'",
				file);
		return false;
	}

	while (fgets(s, STRING_SIZE, f) != NULL) {
		param = strtok(s, "=");
		if (strncmp(param, name, strlen(name)) == 0) {
			e = strtok(NULL, "=");
			if (e[strlen(e) - 1] == '\n')
				e[strlen(e) - 1] = '\0';
			strcpy(value, e);
			fclose(f);
			return true;
		}
	}
	unlock_file(fd);
	fclose(f);
	return false;
}

/*
 * lock a file given by fd
 */
int lock_file(unsigned int fd)
{
	/* get exclusive lock */
	struct flock lock;
	lock.l_type = F_WRLCK;
	lock.l_whence = SEEK_SET;
	lock.l_start = 0;
	lock.l_len = 0;

	return fcntl (fd, F_SETLKW, &lock);
}

/*
 * unlock a file given by fd
 */
int unlock_file(unsigned int fd)
{
	struct flock lock;
	lock.l_type = F_UNLCK;
	lock.l_whence = SEEK_SET;
	lock.l_start = 0;
	lock.l_len = 0;

	return fcntl (fd, F_SETLKW, &lock);
}

int exec_command(char *command)
{
	syslog(LOG_DEBUG, " �--- exec_command (%s): %s",
			safe_system(command) == -1 ? "ERROR" : "OK",
			command);
}

