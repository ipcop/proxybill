/*
 * ProxyBill fw control daemon - proxybill-fwd
 *
 * Copyright (c) 2006, Reto Buerki <reet@codelabs.ch>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * This program is distributed under the terms of the GNU General Public
 * Licence.  See the file COPYING for details.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <syslog.h>
#include "setuid.h"

void print_usage(char *argv)
{
	printf("%s [command]\n", argv);
	printf(" -> command is one of the following:\n");
	printf("\tstart\t:\t start the fw handler\n");
	printf("\trestart\t:\t restart the fw handler\n");
	printf("\tstop\t:\t stop the fw handler\n");
}

int main(int argc, char **argv)
{
	if (argc <=1) {
		print_usage(argv[0]);
		exit(EXIT_FAILURE);
	}

	if (!(initsetuid())) {
		fprintf(stderr, "Could not initsetuid() ... exiting!\n");
		exit(EXIT_FAILURE);
	}

	int status = -1;
	if (strncmp(argv[1], "start", 4) == 0) {
		/* start the (daemon) */
		status = safe_system("/usr/local/bin/proxybill-fwd &");
		if (status != 0)
			fprintf(stderr, "Could not start the daemon!\n");
	} else if (strncmp(argv[1], "restart", 7) == 0) {
		/* send SIGHUP to the deamon */
		status = safe_system("killall -s SIGHUP proxybill-fwd");
		if (status != 0)
			fprintf(stderr, "Could not restart the daemon!\n");
	} else if (strncmp(argv[1], "stop", 4) == 0) {
		/* send SIGTERM to the daemon */
		status = safe_system("killall -s SIGTERM proxybill-fwd");
		if (status != 0)
			fprintf(stderr, "Could not stop the daemon!\n");
	} else
		print_usage(argv[0]);

	exit(status);
}

