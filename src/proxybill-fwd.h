#ifndef PROXYBILL_H
#define PROXYBILL_H

#define true	1
#define false	0

#define expired 2

#define MAXUSER 100
#define NET		6

#define IFACEFILE	"/var/ipcop/ethernet/settings"
#define REDDEVFILE	"/var/ipcop/red/iface"
#define SETTINGFILE	"/var/ipcop/proxybill/settings"
#define CREDITSFILE	"/var/ipcop/proxybill/credits"
#define WATCHDIR	"/var/ipcop/proxybill"
#define DUMPFILE	"/tmp/proxybill.credits"

#define IPTABLES    "/sbin/iptables"

/* credits storage */
typedef struct credit {
	unsigned int state;
	char username[12];
	unsigned int credit;
	char pw[33];
	int timestamp;
	char ip[16];
    char plain[32];
} credit_t;

/* credit status */
static char *state[] = {
	"disabled", "active", "expired"
};

credit_t credits[100];
/* interfaces */
typedef struct iface {
	char name[5];
	char address[16];
	char netaddress[16];
	char netmask[16];
} iface_t;

iface_t red_iface;
iface_t green_iface;
iface_t client_iface;

/* track BLUE network */
int is_blue = false;

/* simple mutex */
int is_writing = false;

/* functions */
void changehandler(int sig);
int update_fw( void );
int read_credits ( void );
int check_credits ( unsigned int clear );
void dump_credits ( void );
int fdump_credits( char*);
int init_system ( void );
int read_iface_info ( void );
int clean_system ( void );
int get_value ( char*, char*, int );
int get_setting ( char*, char*, char* );
int lock_file ( unsigned int );
int unlock_file ( unsigned int );
int exec_command ( char* );

#endif
