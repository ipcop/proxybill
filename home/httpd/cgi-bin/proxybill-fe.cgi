#!/usr/bin/perl
#
# ProxyBill, a user-based portal system for IPCop
# Copyright (c) 2006, Reto Buerki <reet@codelabs.ch>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
use strict;

# enable only the following on debugging purpose
#use warnings;
#use CGI::Carp 'fatalsToBrowser';

# we need MD5 for password hashing
use Digest::MD5 qw(md5 md5_hex md5_base64);

require '/var/ipcop/general-functions.pl';
require "${General::swroot}/proxybill/func.pl";
require "${General::swroot}/lang.pl";
require "${General::swroot}/header.pl";

# files where we store our data
my $setting = "${General::swroot}/proxybill/settings";
our $datafile = "${General::swroot}/proxybill/credits";

my %settings = ();
my %credits = ();

$credits{'ACTION'}   = '';
$credits{'KEY1'}     = '';
$credits{'ACTIVE'}   = '';
$credits{'USERNAME'} = '';
$credits{'PASSWORD'} = '';
$credits{'CREDITS'}  = '';
$credits{'TS'}       = '';
$credits{'IP'}       = '';
$credits{'SHOWPW'}   = 0;
$credits{'PLAIN'}    = '';

# get GUI values
&Header::getcgihash(\%credits);
# read settings
&General::readhash($setting, \%settings);

my $errormessage = '';

# load data
our @current = ();
if(open(FILE, "$datafile")) {
	@current = <FILE>;
	close (FILE);
}

##[funcs]#######################################################################

if ($credits{'ACTION'} eq $Lang::tr{'remove'}) {
	splice (@current,$credits{'KEY1'},1);
	open(FILE, ">$datafile") or die 'ProxyBill datafile error';
	print FILE @current;
	close(FILE);
	$credits{'KEY1'} = '';
	#system("/usr/local/bin/setipbillaccess");
	&General::log("Removed a user for ProxyBill");
	&WriteDataFile;
}

if ($credits{'ACTION'} eq $Lang::tr{'addcredits'}) {
	my $line = @current[$credits{'KEY1'}];
	#chomp($line);
	my @temp = split(/\,/,$line);
	$credits{'ACTIVE'}   = $temp[0];
	$credits{'USERNAME'} = $temp[1];
	$credits{'CREDITS'}  = $temp[2];
	$credits{'PASSWORD'} = $temp[3];
	$credits{'TS'}       = $temp[4];
	$credits{'IP'}       = $temp[5];
    $credits{'PLAIN'}    = $temp[6];

}

if ($credits{'ACTION'} eq $Lang::tr{'generate'}) {
	$credits{'USERNAME'} = &CreateRandomUser();
	$credits{'PASSWORD'} = &CreateRandomPw();
	$credits{'CREDITS'}  = $settings{'DEFAULTCREDIT'} * $settings{'CREDITMUX'};
	$credits{'SHOWPW'}   = 1;
}

if ($credits{'ACTION'} eq $Lang::tr{'add'}) {
	if ($credits{'USERNAME'} eq '') {
		$errormessage = "Please provide a valid username!";
	}
	$credits{'USERNAME'} = &Header::cleanhtml($credits{'USERNAME'});

	if ($credits{'PASSWORD'} eq '') {
		$errormessage = "Empty passwords are not allowed!";
	}
	# hardcoded: passwords must be a least 6 chars long
	if (length($credits{'PASSWORD'}) < 6) {
		$errormessage = "Passwords must have a least 6 chars";

	}
	if (!($credits{'CREDITS'} =~ /^\d+$/)) {
		$errormessage = "Credits must be numeric!";
	}

	if ($credits{'KEY1'} eq '' && !&CheckUserID($credits{'USERNAME'})) {
		$errormessage = "Users must be unique! ('$credits{'USERNAME'}' exists)";
	}
	# if no state given, assume inactive
	if ($credits{'ACTIVE'} eq '') {
		$credits{'ACTIVE'} = 0;
	}

	unless ($errormessage) {
        # calculate credits in seconds
        my $credits_s = $credits{'CREDITS'} * $settings {'CREDITMUX'};

		# add or edit?
		if ($credits{'KEY1'} eq '') {
			my $password = md5_hex($credits{'PASSWORD'});

			unshift(@current, "$credits{'ACTIVE'},$credits{'USERNAME'},$credits_s,$password,0,-,$credits{'PASSWORD'}\n");
			&General::log("Added user $credits{'USERNAME'} for ProxyBill");
		} else {
			@current[$credits{'KEY1'}] = "$credits{'ACTIVE'},$credits{'USERNAME'},$credits_s,$credits{'PASSWORD'},$credits{'TS'},$credits{'IP'},$credits{'PLAIN'}\n";
			$credits{'KEY1'} = '';
			&General::log("Updated user $credits{'USERNAME'} for ProxyBill");
		}
		%credits = ();
		&WriteDataFile;
	}
}

##[MAIN]########################################################################


&Header::showhttpheaders();

# html stuff
print <<END
<html>
<head>
 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
 <meta http-equiv="refresh" content="60; URL="proxybill-fe.cgi">
 <style type="text/css">\@import url(/include/proxybill.css);</style>
 <title>ProxyBill: User Control Interface</title>
 <SCRIPT LANGUAGE="Javascript">
 <!--
 function checkbox_value()
 {
     if ( document.add_user.ACTIVE.checked == true ) {
         document.add_user.ACTIVE.value = 1;
     }
 }
 function open_window(page, width, height, sbar)
 {
     var options ="dependent=yes, scrollbar="+sbar+", width="+width +", height="+height;
     window.open(page, "Full_View", options);
 }
 -->

 </SCRIPT>

</head>
<body>
<br>
<fieldset>
<table width='100%'>
<tr>
	<td><img src='/images/header_proxybill_adm.png' alt='ProxyBill Admin'></td>
</tr>
<tr>
    <td style="border:1px solid black;" bgcolor="#d7d8e8">
END
;

&Header::openbigbox('100%', 'center');

# error status message
if ($errormessage) {
	&Header::openbox('100%', 'left', $Lang::tr{'error messages'});
	print "<FONT CLASS='base'>$errormessage</FONT>\n";
	&Header::closebox();
}

# Users & Credits
&Header::openbox('100%', 'left', $Lang::tr{'add user'});
print <<END
<form method='post' name="add_user" action='$ENV{'SCRIPT_NAME'}'>
<input type='hidden' name='KEY1' value='$credits{'KEY1'}' />
<input type='hidden' name='TS' value='$credits{'TS'}' />
<input type='hidden' name='IP' value='$credits{'IP'}' />
<input type='hidden' name='PLAIN' value='$credits{'PLAIN'}' />

<table width='650px'>
<tr>
	<td width='300px'>
	$Lang::tr{'state'} ($Lang::tr{'state_active'}/$Lang::tr{'state_disabled'})
	</td><td width='350px'>
END
;
	# enabled?
	if ($credits{'ACTIVE'} == 1) {
		print "<input type='checkbox' name='ACTIVE' value='1' checked>";
	} elsif ($credits{'ACTIVE'} == 0 &&
			length($credits{'IP'}) > 2 &&
			length($credits{'TS'}) > 2) {
		print "<input type='checkbox' name='ACTIVE' onClick='checkbox_value()' value='0'>";
	} else {
		print "<input type='checkbox' disabled>";
		print "<input type='hidden' name='ACTIVE' value='0'>";
	}

print <<END
	</td>

</tr>
<tr>
	<td width='300px'>$Lang::tr{'username'}:&nbsp;</td>
	<td width='350px'>
	 <input type='text' name='USERNAME' value='$credits{'USERNAME'}'>
	</td>
</tr>
<tr>
	<td width='300px'>$Lang::tr{'password'}&nbsp;</td>
	<td width='350px'>
END
;
	# disable pw change for now
	my $show_pw = "password";
	if ($credits{'SHOWPW'}) {
		$show_pw = "text";
	}
	if (length($credits{'PASSWORD'}) > 0) {
		print "<input type='$show_pw' value='$credits{'PASSWORD'}' disabled>";
		print "<input type='hidden' name='PASSWORD' value='$credits{'PASSWORD'}'>";
	} else {
		print "<input type='password' name='PASSWORD' value='$credits{'PASSWORD'}'>";
	}

	# display credits in appropriate unit.
	my $credits_u;
	if ($credits{'CREDITS'} eq '') {
		$credits_u = $settings{'DEFAULTCREDIT'};
	} else {
		$credits_u = int(($credits{'CREDITS'} / $settings{'CREDITMUX'}) + .5);
	}
    # show tip about which unit is in use
	my $unit;
    if ($settings{'CREDITMUX'} == 1) {
		$unit = "seconds";
	} elsif ($settings{'CREDITMUX'} == 60) {
		$unit = "minutes";
	} elsif ($settings{'CREDITMUX'} == 3600) {
		$unit = "hours";
	} else {
		$unit = "custom";
	}
print <<END
	</td>
</tr>
<tr>
	<td widht='300px'>$Lang::tr{'credits'}:&nbsp;<img src='/blob.gif' align='top' alt='*'></td>
	<td width='350px'>
	 <input type='text' name='CREDITS' value='$credits_u'>
	</td>
</tr>
</table>
<hr>
<table>
<tr>
	<td class='base' width='50%'><img src='/blob.gif' align='top' alt='*'>
	&nbsp;$Lang::tr{'unit tip'}$Lang::tr{$unit}&nbsp;($settings{'CREDITMUX'})</td>
	<td width='10%'></td>
	<td align='center'>
	 <input type='submit' name='ACTION' value='$Lang::tr{'add'}'>
	</td>
	<td align='center'>
	 <input type='submit' name='ACTION' value='$Lang::tr{'generate'}'>
	<td align='center'>
	 <input type='reset' value='$Lang::tr{'reset'}'>
	</td>

</tr>
</table>
</form>
END
;

&Header::closebox();

# existing users & credits box
&Header::openbox('100%', 'left', $Lang::tr{'current users'});
print <<END
<table width='100%'>
<tr>
    <th>$Lang::tr{'state'}</th>
	<th>$Lang::tr{'username'}</th>
	<th>$Lang::tr{'credits'}</th>
	<th>$Lang::tr{'ip'}</th>
	<th colspan="3">$Lang::tr{'action'}</th>
</tr>
END
;
my $key = 0;
foreach my $line (@current) {
	#chomp($line);
	my @temp = split(/\,/,$line);

	# color lines
	if ($credits{'KEY1'} eq $key) {
		print "<tr bgcolor='${Header::colouryellow}'>";
	} elsif ($key % 2) {
		print "<tr bgcolor='${Header::table2colour}'>";
	} else {
		print "<tr bgcolor='${Header::table1colour}'>";
	}

	# indexes:
	# ------------------------------------
	# 0 - state
	# 1 - user
	# 2 - credits
	# 3 - password
	# 4 - timestamp
	# 5 - ip address
    # 6 - pw (plain)
	# ------------------------------------

	# state
	if ($temp[0] == 0) {
		print "<td align='center' width='5%'>";
		print "<img alt='disabled' src='/images/state_disabled.png' />";
		print "</td>";
	} elsif ($temp[0] == 1) {
		print "<td align='center' width='5%'>";
		print "<img alt='active' src='/images/state_active.png' />";
		print "</td>";
	} elsif ($temp[0] == 2) {
		print "<td align='center' width='5%'>";
		print "<img alt='expired' src='/images/state_expired.png' />";
		print "</td>";
	}

	print "<td class='base' width='25%'>$temp[1]</td>";

	my $credits_u = &PBill::TimeFromSec($temp[2]);

	print "<td class='base' width='25%'>$credits_u</td>";

    print <<END
<td class='base' width='30%'>$temp[5]</td>
<td align='center' width='5%' valign='top'>
 <a href='javascript: open_window("proxybill-print.cgi?credits=$temp[2]&pw=$temp[6]&name=$temp[1]&mux=$settings{'CREDITMUX'}&price=$settings{'PRICE'}&curr=$settings{'CURRENCY'}", 520, 190, "yes");'>
  <img src='/images/pb_print.png' alt='$Lang::tr{'printcredits'}' title='$Lang::tr{'printcredits'}' />
 </a>
</td>
<td align='center' width='5%'>
 <form method='post' action='$ENV{'SCRIPT_NAME'}'>
  <input type='hidden' name='ACTION' value='$Lang::tr{'addcredits'}'>
  <input type='image' name='$Lang::tr{'addcredits'}' src='/images/pb_addcredits.png' alt='$Lang::tr{'addcredits'}' title='$Lang::tr{'addcredits'}'>
  <input type='hidden' name='KEY1' value='$key'>
 </form>
</td>
<td align='center' width='5%'>
 <form method='post' action='$ENV{'SCRIPT_NAME'}'>
  <input type='hidden' name='ACTION' value='$Lang::tr{'remove'}'>
  <input type='image' name='$Lang::tr{'remove'}' src='/images/pb_trash.png' alt='$Lang::tr{'remove'}' title='$Lang::tr{'remove'}'>
  <input type='hidden' name='KEY1' value='$key'>
 </form>
</td>
</tr>
END
;
	$key++;
}
print "</table>";

# if we have entries, display legend
if ($key) {
	print <<END
<table width='50%'>
<tr>
	<td class='boldbase'>&nbsp;<b>$Lang::tr{'action_legend'}:&nbsp;</b></td>
	<td><img src='/images/pb_trash.png' alt='$Lang::tr{'remove'}'></td>
	<td class='base'>$Lang::tr{'remove'}</td>
	<td><img src='/images/pb_addcredits.png' alt='$Lang::tr{'addcredits'}'></td>
	<td class='base'>$Lang::tr{'addcredits'}</td>
</tr>
</table>
<table width='50%'>
<tr>
	<td class='boldbase'>&nbsp;<b>$Lang::tr{'state'}:&nbsp;</b></td>
	<td><img src='/images/state_disabled.png' alt='$Lang::tr{'state_disabled'}'></td>
	<td class='base'>$Lang::tr{'state_disabled'}</td>
	<td><img src='/images/state_active.png' alt='$Lang::tr{'state_active'}'></td>
	<td class='base'>$Lang::tr{'state_active'}</td>
	<td><img src='/images/state_expired.png' alt='$Lang::tr{'state_expired'}'></td>
	<td class='base'>$Lang::tr{'state_expired'}</td>
</tr>
</table>

END
;
}

&Header::closebox();

&Header::closebigbox();

&Header::closebigbox();

print <<END
	</td>
</tr>
</table>
</fieldset>
</body>
</html>
END
;

##[subs]########################################################################

sub WriteDataFile {
	open(FILE, ">$datafile") or die 'ProxyBill data file error';
	print FILE @current;
	close (FILE);
}

sub CheckUserID {
	my $userID = shift;
	print "$userID";
	foreach my $l (@current) {
		#chomp($l);
		my @t = split(/\,/,$l);
		if ($t[1] eq $userID) {
			return 0;
		}
	}
	return 1;
}

sub CreateRandomUser {
	my $RandomUserMin = 8;
	my $RandomUserMax = 8;

	my $letters = int(rand $RandomUserMax)+1;
	my @newname="";

	if ($letters < $RandomUserMin) { $letters = $RandomUserMin; }

	for my $x (1..$letters) { $newname[$x-1] =  pack "C", int(rand 26)+97; }

	return join(/1/, @newname);
}

sub CreateRandomPw {
	my $RandomPwMin = 8;
	my $RandomPwMax = 8;

	my $letters = int(rand $RandomPwMax)+1;
	my @newpw="";

	if ($letters < $RandomPwMin) { $letters = $RandomPwMin; }

	for my $x (1..$letters) { $newpw[$x-1] =  pack "C", int(rand 26)+97; }

	return join(/1/, @newpw);
}

################################################################################

