#!/usr/bin/perl
#
# ProxyBill, a user-based portal system for IPCop
# Copyright (c) 2006, Reto Buerki <reet@codelabs.ch>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
use strict;
use CGI;

# enable only the following on debugging purpose
#use warnings;
use CGI::Carp 'fatalsToBrowser';

require '/var/ipcop/general-functions.pl';
require "${General::swroot}/lang.pl";
require "${General::swroot}/header.pl";

my $cgi = new CGI;

my %data = ();

$data{'USERNAME'} = $cgi->param('name') || undef;
$data{'PASSWORD'} = $cgi->param('pw') || undef;
$data{'CREDITS'}  = $cgi->param('credits') || undef;
$data{'MUX'}      = $cgi->param('mux') || undef;
$data{'PRICE'}    = $cgi->param('price') || undef;
$data{'CURRENCY'} = $cgi->param('curr') || undef;

##[MAIN]########################################################################

&Header::showhttpheaders();

# calculate time string
my $cleft = &TimeFromSec($data{'CREDITS'});

# calculate total price
my $price_tot = ($data{'CREDITS'} / $data{'MUX'}) * $data{'PRICE'};
# round prices
my $price  = sprintf("%.2f", $data{'PRICE'});
$price_tot = sprintf("%.2f", $price_tot);

# show appropriate unit
my $unit;
if ($data{'MUX'} == 1) {
    $unit = "seconds";
} elsif ($data{'MUX'} == 60) {
    $unit = "minutes";
} elsif ($data{'MUX'} == 3600) {
    $unit = "hours";
} else {
    $unit = "custom";
}

# html stuff
print <<END
<html>
<head>
 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
 <style type="text/css">\@import url(/include/proxybill.css);</style>
 <title>ProxyBill: Print-User</title>
</head>
<body>
<br>
<fieldset>
<table width='100%'>
<tr>
  <td><h3>Proxy: User-Details</h3></td>
</tr>
<tr>
  <td style="border:1px solid black;" bgcolor="#d7d8e8">
    <table align='left' width='100%'>
      <tr><td>$Lang::tr{'username'}:</td><td>$data{'USERNAME'}</td></tr>
      <tr><td>$Lang::tr{'password'}</td><td>$data{'PASSWORD'}</td></tr>
      <tr><td>$Lang::tr{'credits'}:</td><td>$cleft</td></tr>
      <tr><td>$Lang::tr{'price'}&nbsp;($Lang::tr{$unit}):</td>
          <td>$price&nbsp;$data{'CURRENCY'}</td>
      </tr>
      <tr><td>$Lang::tr{'price_tot'}:</td><td>$price_tot&nbsp;$data{'CURRENCY'}</td></tr>
      <tr><td>$Lang::tr{'activation'}:</td>
          <td>https://$ENV{'SERVER_NAME'}:445/cgi-bin/proxybill-login.cgi</td>
      </tr>
    </table>
  </td>
</tr>
</table>
</form>
<div align='center'>
  <a href='javascript: window.print();'>Print</a>
</div>
</fieldset>
</body>
</html>
END
;

##[subs]########################################################################

sub TimeFromSec {
    my $timer = '';
    my $sec = 0.0;
    my $min = 0.0;
    my $hour = 0.0;
    my $time = shift;
    if ($time >= 60) {
        $min = int($time / 60);
        $sec = $time % 60;
        if ($min >= 60) {
            $hour = int($time / (60*60));
            $min = $min % 60;				
        }
    } else {
        $sec = $time;
    }
    $timer = sprintf("%2.0fh : %2.0fm : %2.0fs", $hour,$min,$sec);    	
    return($timer);
}

################################################################################

