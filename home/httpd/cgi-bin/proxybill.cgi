#!/usr/bin/perl
#
# ProxyBill, a user-based portal system for IPCop
# Copyright (c) 2006, Reto Buerki <reet@codelabs.ch>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
use strict;

# enable only the following on debugging purpose
#use warnings;
#use CGI::Carp 'fatalsToBrowser';

require '/var/ipcop/general-functions.pl';
require "${General::swroot}/lang.pl";
require "${General::swroot}/header.pl";

# files where we store our data
my $setting = "${General::swroot}/proxybill/settings";

# read IPCop network settings
my %netsettings=();
&General::readhash("${General::swroot}/ethernet/settings", \%netsettings);

my %settings = ();
$settings{'ACTION'}         = '';
$settings{'CREDITMUX'}      = '';
$settings{'CNET'}           = '';
$settings{'DEFAULTCREDIT'}  = '';
$settings{'PRICE'}          = '';
$settings{'CURRENCY'}       = '';

# get GUI values
&Header::getcgihash(\%settings);

my $errormessage = '';

##[funcs]#######################################################################

if ($settings{'ACTION'} eq $Lang::tr{'save'}) {
	if ($settings{'CNET'} eq '') {
		$errormessage = "No valid client network given!";
	}
    # validate values
	if (!($settings{'CREDITMUX'} =~ /^\d+$/)) {
		$errormessage = "$settings{'CREDITMUX'} is not a valid multiplexer!";
	}
	if (!($settings{'DEFAULTCREDIT'} =~ /^\d+$/)) {
		$errormessage = "$settings{'DEFAULTCREDIT'} is not a valid default credit!";
	}
	if (!($settings{'PRICE'} =~ /^([0-9]*\.)?[0-9]+$/)) {
		$errormessage = "$settings{'PRICE'} is not a valid price per unit!";
	}

	unless ($errormessage) {
		# save settings
		&General::writehash($setting, \%settings);
		# (re)start proxybill-fwd
		if ( (my $pid = `pidof proxybill-fwd`) == "") {
			system("/usr/local/bin/proxybill-ctrl start");
		} else {
			system("/usr/local/bin/proxybill-ctrl restart");
		}
	}
}

if ($settings{'ACTION'} eq $Lang::tr{'stop'}) {
    # stop proxybill-fwd if running
    if ( (my $pid = `pidof proxybill-fwd`) != "") {
        system("/usr/local/bin/proxybill-ctrl stop");
    }
}

##[MAIN]########################################################################


&General::readhash($setting, \%settings);

# assign checked options
my %checked = ();
$checked{'ENABLED_NET'}{'GREEN'} = '';
$checked{'ENABLED_NET'}{'BLUE'} = '';
$checked{'ENABLED_NET'}{$settings{'CNET'}} = 'checked';

# assign selected options
my %selected = ();
$selected{'EUR'} = '';
$selected{'USD'} = '';
$selected{'CHF'} = '';
$selected{$settings{'CURRENCY'}} = 'selected="selected"';

&Header::showhttpheaders();

&Header::openpage('ProxyBill', 1, '');

&Header::openbigbox('100%', 'left');

# error status message
if ($errormessage) {
	&Header::openbox('100%', 'left', $Lang::tr{'error messages'});
	print "<FONT CLASS='base'>$errormessage</FONT>\n";
	&Header::closebox();
}

# status information
&Header::openbox('100%', 'left', $Lang::tr{'pb status'});
my $pid = `pidof proxybill-fwd`;

if ($pid == "") {
	print "<b>ProxyBill firewall handler is not running!</b><br />";
} else {
	print <<END
	<table width='100%'>
	<tr>
		<td width='25%' class='base'>$Lang::tr{'daemon pid'}:&nbsp;</td>
		<td>
	 	 <b>$pid</b>
		</td>
	</tr>
	</table>
END
;
}

&Header::closebox();

# general settings
&Header::openbox('100%', 'left', $Lang::tr{'general settings'});
print <<END
<form method='post' action='$ENV{'SCRIPT_NAME'}'>
<table width='100%'>
<tr>
	<td width='25%' class='base'>$Lang::tr{'client net'}&nbsp;</td>
	<td>
END
;
	print "<input type='radio' name='CNET' value='GREEN' $checked{'ENABLED_NET'}{'GREEN'}>GREEN<br>";
	if ($netsettings{'BLUE_DEV'}) {
		print "<input type='radio' name='CNET' value='BLUE' $checked{'ENABLED_NET'}{'BLUE'}>
			BLUE<br>";
	}

print <<END
	</td>
</tr>
<tr>
	<td width='25%' class='base'>$Lang::tr{'mux'} <img src='/blob.gif' alt='*' /></td>
	<td>
	 <input type='text' name='CREDITMUX' value='$settings{'CREDITMUX'}'>
	</td>
</tr>
<tr>
	<td width='25%' class='base'>$Lang::tr{'dcredit'}&nbsp;</td>
	<td>
	 <input type='text' name='DEFAULTCREDIT' value='$settings{'DEFAULTCREDIT'}'>
	</td>
</tr>
<tr>
	<td width='25%' class='base'>$Lang::tr{'price'}&nbsp;</td>
	<td>
	 <input type='text' name='PRICE' value='$settings{'PRICE'}'>
	</td>
</tr>
<tr>
	<td width='25%' class='base'>$Lang::tr{'currency'}&nbsp;</td>
	<td>
     <select name='CURRENCY' size='1'>
       <option $selected{'EUR'}>EUR</option>
       <option $selected{'USD'}>USD</option>
       <option $selected{'CHF'}>CHF</option>
     </select>
	</td>
</tr>
<tr>
	<td>Frontend:&nbsp;</td>
	<td><a href="proxybill-fe.cgi">$Lang::tr{'open'}</a></td>
</tr>
</table>
<hr>
<table table width='100%'>
<tr>
	<td class='base' width='40%'>
	  <img src='/blob.gif' align='top' alt='*' /> $Lang::tr{'multiplex tip'}
	</td>
	<td width='30%' align='center'>
	 <input type='submit' name='ACTION' value='$Lang::tr{'save'}'>
    </td>
	<td width='30%' align='center'>
	 <input type='submit' name='ACTION' value='$Lang::tr{'stop'}'>
    </td>
</tr>
</table>
</form>
END
;

&Header::closebox();

&Header::closebigbox();

&Header::closepage();


################################################################################

