#!/usr/bin/perl
#
# ProxyBill, a user-based portal system for IPCop
# Copyright (c) 2006, Reto Buerki <reet@codelabs.ch>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
use strict;

# enable only the following on debugging purpose
#use warnings;
use CGI::Carp 'fatalsToBrowser';

# we need MD5 for password hashing
use Digest::MD5 qw(md5 md5_hex md5_base64);
# session management
use CGI qw(:standard);

require '/var/ipcop/general-functions.pl';
require "${General::swroot}/proxybill/func.pl";
require "${General::swroot}/lang.pl";
require "${General::swroot}/header.pl";

# files where we store our data
my $setting = "${General::swroot}/proxybill/settings";
our $datafile = "${General::swroot}/proxybill/credits";

my %settings = ();
my %login = ();

$login{'ACTION'} = '';
$login{'ACTIVE'} = '';
$login{'USERNAME'} = '';
$login{'PASSWORD'} = '';

# get GUI values
&Header::getcgihash(\%login);

# read settings
&General::readhash($setting,\%settings);

# user handling vars
my $u_id;
my $u_status;
my $u_pw;
my $u_name;
my $u_credits;
my $u_ip;
my $u_ts;
my $u_plain;

my $errormessage = '';

# load data
our @current = ();
if(open(FILE, "$datafile")) {
	@current = <FILE>;
	close (FILE);
}

# check existing cookie
my $cookie = cookie("proxybill");
if ($cookie ne '') {
	# a cookie exists -> check it
	my $user = substr($cookie, 0, index($cookie, ':'));
	if (&ReadUserInfo($user) eq -1) {
		# user does not exist -> delete cookie
		&DeleteCookie;
	}
}

##[funcs]#######################################################################

if ($login{'ACTION'} eq $Lang::tr{'login'}) {

	if ($login{'USERNAME'} eq '') {
		$errormessage = "Please provide a valid username!";
	} elsif ($login{'PASSWORD'} eq '') {
		$errormessage = "Please provide a password!";
	}

	unless ($errormessage) {

		# read user info
		my $counter = &ReadUserInfo($login{'USERNAME'});

		# check password and login
		my $given_pw = md5_hex($login{'PASSWORD'});
		if ($given_pw != $u_pw || $counter eq -1) {
			$errormessage = "Access denied!";
		} else {

			# get user IP
			my $ip = $ENV{'REMOTE_ADDR'};
			if ($ip eq '') {
				$errormessage ="Could not get client IP!";
			} else {
				# register IP
				@current[$counter] = "$u_status,$u_name,$u_credits,$u_pw,$u_ts,$ip,$u_plain";
				&WriteDataFile;
				&General::log("Login (ProxyBill): user '$u_name' (credits: $u_credits)");
				# set cookie
				my $hash = md5_hex($u_pw + $ip);
				my $to_set = cookie(-name=>"proxybill",
						-value=>"$u_name:$hash",
						-expires=>'+3600S',
						-path=>'/');

				print header(-cookie=>$to_set,
						-Location=>$ENV{'SCRIPT_NAME'},
						-status=>'302 Found');
				exit;
			}
		}
		%login = ();
	}
}

if ($login{'ACTION'} eq $Lang::tr{'logout'}) {
	&DeleteCookie;
}

if ($login{'ACTION'} eq $Lang::tr{'activate'}) {
	&ActivateUser($u_name);
	&General::log("Activate (ProxyBill): user '$u_name' (credits: $u_credits)");
	&ReloadPage;
}

if ($login{'ACTION'} eq $Lang::tr{'suspend'}) {
	&SuspendUser($u_name);
	&General::log("Suspend (ProxyBill): user '$u_name' (credits: $u_credits)");
	&ReloadPage;
}

##[MAIN]########################################################################

&Header::showhttpheaders();

# html stuff
print <<END
<html>
<head>
  <meta http-equiv="refresh" content="30; URL="proxybill-login.cgi">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <style type="text/css">\@import url(/include/proxybill.css);</style>
  <title>Proxy /</title>
  <SCRIPT LANGUAGE="Javascript">
  <!--
END
;

# status message
if ($u_status ne '') {
	print "var status    = $u_status;";
	print "var credits   = $u_credits;";
	print "var timestamp = $u_ts;";
} else {
	# we must define something
	print "var status     = 3;";
	print "var credits    = 0;";
	print "var timestamp  = 0;";
}

print <<END
  var actualtitle = document.title + " ";
  function time() {
	var now   = new Date();
	var cleft = Math.round((credits + timestamp) - (now.getTime() / 1000.0));
    min = Math.floor(cleft/60);
    sec = cleft % 60;
    hr  = Math.floor(cleft/3600);
    if (sec < 0) {
      min = 0;
      sec = 0;
      hr  = 0;
    }
  }
  function scroll() {
    time();
    var scroller   = "Credits: " +hr + "h : " + min + "m : " + sec + "s";
    var timeout    = setTimeout("scroll()", 1000);
    document.title = scroller;
  }

  if (status == 1) {
    scroll();
  } else if (status == 2) {
    document.title = actualtitle + "expired";
  } else if (status == 3) {
    document.title = actualtitle + "Login";
  } else {
    document.title = actualtitle + "inactive";
  }
  -->
  </SCRIPT>
 </head>
<body>
<br>
<fieldset>
<table width='100%'>
<tr>
	<td><img src='/images/header_proxybill_usr.png' alt='IPBill'></td>
</tr>
<tr>
    <td style="border:1px solid black;" bgcolor="#d7d8e8">
END
;

&Header::openbigbox('100%', 'center');

# error status message
if ($errormessage) {
	&Header::openbox('100%', 'left', $Lang::tr{'error messages'});
	print "<FONT CLASS='base'>$errormessage</FONT>\n";
	&Header::closebox();
}

# if a cookie exists -> compare hashes
if ($cookie ne '') {
	&Header::openbox('100%', 'left', $Lang::tr{'login success'});
	# compare the hashes
	my $hash = substr($cookie, index($cookie, ':')+1);
	if (md5_hex($u_pw + $ENV{'REMOTE_ADDR'}) ne $hash) {
		print "<h1>UNAUTH ACCESS detected!!</h1>";
		&Header::closebox();
		exit;
	} else {
		&DisplayMenu;
	}
	&Header::closebox();

} else {

# LOGIN
&Header::openbox('100%', 'left', $Lang::tr{'user login'});
print <<END
<form method='post' action='$ENV{'SCRIPT_NAME'}'>
<table width='100%'>
<tr>
	<td width='25%' class='base'>$Lang::tr{'username'}:&nbsp;</td>
	<td>
	 <input type='text' name='USERNAME' value='$login{'USERNAME'}'>
	</td>
</tr>
<tr>
	<td class='base'>$Lang::tr{'password'}&nbsp;</td>
	<td>
	 <input type='password' name='PASSWORD' value='$login{'PASSWORD'}'>
	</td>
</tr>
</table>
<hr>
<table width='100%'>
<tr>
	<td width='100%' align='center'>
	 <input type='submit' name='ACTION' value='$Lang::tr{'login'}'>
	</td>
</tr>
</table>
</form>
END
;

&Header::closebox();
}

&Header::closebigbox();

print <<END
	</td>
</tr>
</table>
</fieldset>
</body>
</html>
END
;

##[subs]########################################################################

sub WriteDataFile {
	open(FILE, ">$datafile") or die 'ProxyBill data file error';
	print FILE @current;
	close (FILE);
}

sub ReadUserInfo {
	my $user = shift;
	my $found = 0;
	my $counter = 0;
	my $line;
	foreach $line (@current){
		my @user_entry = split(/\,/,$line);
		if ($user_entry[1] eq $user) {
			$found = 1;
			$u_id      = $counter;
			$u_status  = $user_entry[0];
			$u_name    = $user_entry[1];
			$u_credits = $user_entry[2];
			$u_pw      = $user_entry[3];
			$u_ts      = $user_entry[4];
			$u_ip      = $user_entry[5];
            $u_plain   = $user_entry[6];
			last;
		}
		$counter++;

	}
	if ($found) { return $counter; }
	else { return -1; }
}

sub DeleteCookie {
	my $to_set = cookie(-name=>"proxybill",
			-value=>"",
			-expires=>'-3600S',
			-path=>'/');
	print header(-cookie=>$to_set,
			-Location=>$ENV{'SCRIPT_NAME'},
			-status=>'302 Found');
}

sub ReloadPage {
	print header(-Location=>$ENV{'SCRIPT_NAME'},
			-status=>'302 Found');
}

sub DisplayMenu {
	print "<table align='center' width='80%'>";
	print "<tr>";
	# state
	print "<td>$Lang::tr{'state'}</td>";
	if ($u_status == 0) {
		print "<td class='base'>";
		print "<img alt='disabled' src='/images/state_disabled.png' />";
		print "</td>";
	} elsif ($u_status == 1) {
		print "<td class='base'>";
		print "<img alt='active' src='/images/state_active.png' />";
		print "</td>";
	} elsif ($u_status == 2) {
		print "<td class='base'>";
		print "<img alt='expired' src='/images/state_expired.png' />";
		print "</td>";
	}
	print "</tr><tr>";
	print "<td>$Lang::tr{'username'}</td><td>$u_name</td>";
	print "</tr><tr>";
	# connection active -> display credits in 'real time'

	if ($u_status eq 1) {
		my $cleft = ($u_ts + $u_credits) - time;
		my $printcleft = &PBill::TimeFromSec($cleft);
	   	if ($cleft < 0) {
			print "<td>$Lang::tr{'cleft'}</td><td>0</td>";
		} else {
			print "<td>$Lang::tr{'cleft'}</td><td>$printcleft</td>";
		}
	} elsif ($u_status eq 0) {
                my $cleft = &PBill::TimeFromSec($u_credits);
		print "<td>$Lang::tr{'cleft'}</td><td>$cleft</td>";
	}
 	print "</tr><tr>";
	print "<td>$Lang::tr{'ip'}</td><td>$u_ip</td>";
	print "</tr></table>";

print <<END
<br>
<fieldset>
<legend>$Lang::tr{'action'}</legend>
<table>
 <tr><td>
  <form method='post' action='$ENV{'SCRIPT_NAME'}'>
   $Lang::tr{'logout'}:
   <input type='hidden' name='ACTION' value='$Lang::tr{'logout'}'>
   <input type='image' name='$Lang::tr{'logout'}' src='/images/logout.png' alt='$Lang::tr{'logout'}' title='$Lang::tr{'logout'}'>
  </form>
 </td>
  <td>
END
;
	if ($u_status eq 0) {
		print "<form method='post' action='$ENV{'SCRIPT_NAME'}'>";
		print "$Lang::tr{'activate'}:<input type='hidden' name='ACTION' value='$Lang::tr{'activate'}'>";
		print "<input type='image' name='$Lang::tr{'activate'}' src='/images/state_active.png' alt='$Lang::tr{'activate'}' title='$Lang::tr{'activate'}'>";
		print "</form>";
	} elsif ($u_status eq 1) {
		print "<form method='post' action='$ENV{'SCRIPT_NAME'}'>";
		print "$Lang::tr{'suspend'}:<input type='hidden' name='ACTION' value='$Lang::tr{'suspend'}'>";
		print "<input type='image' name='$Lang::tr{'suspend'}' src='/images/state_disabled.png' alt='$Lang::tr{'suspend'}' title='$Lang::tr{'suspend'}'>";
		print "</form>";
	} else {
		print "<p><i>account expired! (contact admin)</i></p>";
	}
	print "</td></tr>";
	print "</table>";
	print "</fieldset>";
}

sub ActivateUser {
	my $timestamp = time;
	@current[$u_id] = "1,$u_name,$u_credits,$u_pw,$timestamp,$u_ip,$u_plain";
	&WriteDataFile;
}

sub SuspendUser {
	my $cleft = ($u_ts + $u_credits) - time;
	@current[$u_id] = "0,$u_name,$cleft,$u_pw,$u_ts,$u_ip,$u_plain";
	&WriteDataFile;
}

################################################################################

