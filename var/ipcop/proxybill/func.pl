#
# ProxyBill, a user-based portal system for IPCop
# Copyright (c) 2006, Reto Buerki <reet@codelabs.ch>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#

package PBill;

use strict;

my $enabled = 1;

sub TimeFromSec {
    my $timer = '';
    my $sec = 0.0;
    my $min = 0.0;
    my $hour = 0.0;
    my $time = shift;
    if ($time >= 60) {
        $min = int($time / 60);
        $sec = $time % 60;
        if ($min >= 60) {
            $hour = int($time / (60*60));
            $min = $min % 60;
        }
    } else {
        $sec = $time;
    }
    $timer = sprintf("%2.0fh : %2.0fm : %2.0fs", $hour,$min,$sec);
    return($timer);
}

